package ordination;

import java.time.*;
import java.util.ArrayList;

public class DagligFast extends Ordination{

	private Dosis[] doser;

	public DagligFast(
			LocalDate startDen,
			LocalDate slutDen, double morgenAntal,
			double middagAntal, double aftenAntal, double natAntal) {

		super(startDen, slutDen);

		this.doser = new Dosis[4];

		this.doser[0] = this.opretDosis(LocalTime.of(6, 0), morgenAntal);
		this.doser[1] = this.opretDosis(LocalTime.of(12, 0), middagAntal);
		this.doser[2] = this.opretDosis(LocalTime.of(18, 0), aftenAntal);
		this.doser[3] = this.opretDosis(LocalTime.of(12, 0), natAntal);
	}

	public Dosis opretDosis(LocalTime tid, double antal) {
		return new Dosis(tid, antal);
	}

	public Dosis[] getDoser() {
		return this.doser;
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for (Dosis d : this.doser) {
			sum += d.getAntal();
		}
		return sum;
	}

	@Override
	public String getType() {
		return "Daglig Fast";
	}
}
