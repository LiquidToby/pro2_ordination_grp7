package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
    private int antalGangeGivet;
    private ArrayList<LocalDate> dosisDatoer;
    
    public PN(LocalDate startDen, LocalDate slutDen) 
    {
		super(startDen, slutDen);
		antalEnheder = 0;
		antalGangeGivet = 0;
		dosisDatoer = new ArrayList<LocalDate>();
	}

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen
     * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
     * Retrurner false ellers og datoen givesDen ignoreres
     * @param givesDen
     * @return
     */
    public boolean givDosis(LocalDate givesDen) {
        // TODO
    	if (givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen())) 
    	{
    		antalGangeGivet ++;
    		dosisDatoer.add(givesDen);
            return true; 

		}
    	else
    	{
        	
        	return false;
    	}

      
    }

    public double doegnDosis() {
    	
    	double tmpDosis = 0.0;
    	tmpDosis = (antalGangeGivet*antalEnheder)/this.antalDage();
    	
        return tmpDosis;
    }


    public double samletDosis() {
        return doegnDosis() * antalDage();
    }

    /**
     * Returnerer antal gange ordinationen er anvendt
     * @return
     */
    public int getAntalGangeGivet() {
       
        return antalGangeGivet;
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }

	public String getType() {
		
		return "PN";
	}

	public void setAntalEnheder(double antalEnheder) {
		this.antalEnheder = antalEnheder;
	}
	
	

}
